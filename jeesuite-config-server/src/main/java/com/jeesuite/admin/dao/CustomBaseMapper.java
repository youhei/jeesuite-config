package com.jeesuite.admin.dao;

import com.mendmix.mybatis.core.BaseEntity;
import com.mendmix.mybatis.core.BaseMapper;

public interface CustomBaseMapper<T extends BaseEntity> extends BaseMapper<T,Integer> {

}
